using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Hability
{
    Augment,
    Decrease,
    GiantTime,
    Cooldown,
    Nothing

}


public class BigHability : MonoBehaviour
{
    public Hability _big;
    private float _bigDuration = 5f;
    private float _bigAtenuator = 2f;
    private float _velocityBig;
    private float _tamaoOriginal;
    private float _timerBig = 0;

    // Start is called before the first frame update
    void Start()
    {
        _tamaoOriginal = transform.localScale.x;
        _big = Hability.Nothing;
    }

    // Update is called once per frame
    void Update()
    {

        switch (_big)
        {
            case Hability.Augment:
                BigHabilty();
                break;
            case Hability.Decrease:
                DecHab();
                break;
            case Hability.GiantTime:
                TimerBig();
                break;
        }

    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bolet"))
        {
            _big = Hability.Augment;

        }
    }

    public void BigHabilty()
    {
        _velocityBig = _bigAtenuator * Time.deltaTime;
        if (transform.localScale.x < _tamaoOriginal * 3)
        {
            transform.localScale += new Vector3(_velocityBig, _velocityBig, 0);
        }
        else
        {
            _big = Hability.GiantTime;
        }
    }

    public void TimerBig()
    {
        if (_timerBig < _bigDuration)
        {
            _timerBig += Time.deltaTime;
        }
        else
        {
            _big = Hability.Decrease;
            _timerBig = 0;
        }
    }

    public void DecHab()
    {
        _velocityBig = _bigAtenuator * Time.deltaTime;
        if (_tamaoOriginal < transform.localScale.x)
        {
            transform.localScale -= new Vector3(_velocityBig, _velocityBig, 0);
        }
        else
        {
            _big = Hability.Nothing;
        }
    }
}
