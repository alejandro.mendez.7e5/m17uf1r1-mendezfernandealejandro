using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMove : MonoBehaviour
{
    private SpriteRenderer _sprite;
    private Animator _animator;
    private Rigidbody2D _rb;
    private RaycastHit2D _hit;

    public float Speed = 0.03f;
    private float _move;
    private float _force = 5f;
    public bool _isJump = false;

    // Start is called before the first frame update
    void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        _animator.SetBool("JumpKuku", _isJump);
        Run();
        if (Input.GetKey(KeyCode.Space) && !_isJump)
        {
           Jump();
        }

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            _isJump = false;
            _rb.velocity = new Vector2(_rb.velocity.x, 0);
        }
    }

    private void Run()
    {
        _move = Input.GetAxis("Horizontal");
        _animator.SetBool("RunKuku", _move != 0.0f);
        transform.position = new Vector3(Speed * _move + transform.position.x, transform.position.y, 0);
        if (_move < 0.0f) _sprite.flipX = true;
        else if (_move > 0.0f) _sprite.flipX = false;
    }

    private void Jump() 
    {
        _rb.AddForce(Vector2.up *_force, ForceMode2D.Impulse);
        _isJump=true;
    }
}
