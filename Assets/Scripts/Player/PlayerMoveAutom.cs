using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveAutom : MonoBehaviour
{
    private SpriteRenderer _sprite;
    private Animator _animator;
    private RaycastHit2D _hitGround;
    private RaycastHit2D _hitFire;

    private bool _raycast = false;
    private float _speed = 0.02f;
    private int _direction = 1; //Value 1 --> To right || Value -1 --> To left
    // Start is called before the first frame update
    void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        MoveAuto();
        _hitGround = Physics2D.Raycast(this.transform.position, new Vector3(_direction, -1, 0));
        _hitFire = Physics2D.Raycast(this.transform.position, new Vector3(_direction, 1, 0));
        //Debug.DrawRay(transform.position, new Vector3(_direction, 1, 0), Color.red);

        if (!_hitGround) _raycast = true;
        else if (_hitGround.collider.tag == "Ground") _raycast = false;

        if(_hitFire.collider.tag == "FireBall") _raycast = true;
    }

    private void MoveAuto()
    {

        if (_raycast)
        {
            _direction *= -1;
            _speed *= -1;
            _sprite.flipX = (_sprite.flipX) ? false : true;
            //Cambia de direccion el raycast!
        }

        transform.position += new Vector3(_speed, 0, 0);
        _animator.SetBool("RunKuku", _speed != 0.0f);
    }
}
