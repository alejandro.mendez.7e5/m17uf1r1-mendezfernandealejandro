using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FireBall : MonoBehaviour
{
    private Animator _animator;
    private int _win = 0;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(collision.gameObject);
            PlayerPrefs.SetInt("win", _win);
            SceneManager.LoadScene("Results");
        }
        if (collision.gameObject.CompareTag("Ground"))
        {
            GameManager.Instance.CountFireBall();
            Destroy(this.gameObject.GetComponent<Rigidbody2D>());
            Destroy(this.gameObject.GetComponent<CapsuleCollider2D>());
            _animator.SetBool("Explosion", true);
            Destroy(this.gameObject, 0.5f);

        }
    }
}
