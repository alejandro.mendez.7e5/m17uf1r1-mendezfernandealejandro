using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Barranco : MonoBehaviour
{
    private string _name;
    private int _win = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _name = PlayerPrefs.GetString("nom");
            Destroy(collision.gameObject);
            PlayerPrefs.SetString("nom", _name);
            PlayerPrefs.SetInt("win", _win);
            SceneManager.LoadScene("Results");
        }
    }
}
