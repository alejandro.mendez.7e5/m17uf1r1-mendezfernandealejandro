using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickSpawnFire : MonoBehaviour
{
    [SerializeField] 
    private Camera _camera;

    [SerializeField]
    private float timeBetweenFireBalls = 1f;
    private float cooldownFire = 0;

    [SerializeField] 
    private GameObject _fireball;

    private void Start()
    {

    }


    void Update()
    {
        if (cooldownFire <= 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                var click = _camera.ScreenToWorldPoint(Input.mousePosition); 
                click[1] = 10;
                click[2] = 0; 

                Instantiate(_fireball, click, new Quaternion(0f, 0f, 0f, 0f));
                cooldownFire = timeBetweenFireBalls;
            }
        }
        else cooldownFire -= Time.deltaTime;
    }
}
