using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private float _count = 60;
    private string _name;
    private int _win = 1;
    // Start is called before the first frame update
    void Start()
    {
        _name = PlayerPrefs.GetString("nom");
    }

    // Update is called once per frame
    void Update()
    {
        if(_count < 1)
        {
            PlayerPrefs.SetInt("win", _win);
            SceneManager.LoadScene("Results");
        }
        this.gameObject.GetComponent<Text>().text = formatearTiempo();
    }

    public string formatearTiempo()
    {
        _count -= Time.deltaTime;

        //Formateo minutos y segundos a dos d�gitos
        string minutos = Mathf.Floor(_count / 60).ToString("00");
        string segundos = Mathf.Floor(_count % 60).ToString("00");

        //Devuelvo el string formateado con : como separador
        return minutos + ":" + segundos;

    }
}
