using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFire : MonoBehaviour
{
    public GameObject _fireball;
    public GameObject _champi;
    private float _timer = 0;
    private float _cooldown = 0.50f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float pos = Random.RandomRange(-11f, 11f);
        _timer += Time.deltaTime;
        if (_timer > _cooldown)
        {
            Instantiate(_fireball, new Vector3(pos, 10, 0), Quaternion.identity);
            _timer = 0;
        }

        int num = Random.Range(0, 7000);
        //Debug.Log(num);
        if(num == 4)
        {
            Instantiate(_champi, new Vector3(pos, 10, 0), Quaternion.identity);
        }
        
    }
}
