using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ManagerMenu : MonoBehaviour
{
    private GameObject _txt;
    private InputField _inputField;
    private string _name;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ControllerHero()
    {
        _txt = GameObject.Find("Name");
        _inputField = _txt.GetComponent<InputField>();
        _name = _inputField.text;
        if(_name != "")
        {
            PlayerPrefs.SetString("nom", _name);
            PlayerPrefs.SetInt("scene", 1);
            SceneManager.LoadScene("SampleScene");
        }
    }

    public void ControllerFireBall()
    {
        _txt = GameObject.Find("Name");
        _inputField = _txt.GetComponent<InputField>();
        PlayerPrefs.SetInt("scene",10);
        _name = _inputField.text;
        if (_name != "")
        {
            PlayerPrefs.SetString("nom", _name);
            SceneManager.LoadScene("Game2");
        }
    }

    public void SceneMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void NewPlayHero()
    {
        _name = PlayerPrefs.GetString("nom");
        PlayerPrefs.SetString("nom", _name);
        PlayerPrefs.SetInt("scene", 1);
        SceneManager.LoadScene("SampleScene");
    }

    public void NewPlayFire()
    {
        _name = PlayerPrefs.GetString("nom");
        PlayerPrefs.SetString("nom", _name);
        PlayerPrefs.SetInt("scene", 10);
        SceneManager.LoadScene("Game2");
    }
}
