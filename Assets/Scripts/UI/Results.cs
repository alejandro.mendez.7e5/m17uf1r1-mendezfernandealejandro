using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Results : MonoBehaviour
{
    private GameObject _obj;
    private GameObject _obj1;
    private GameObject _obj2;
    private GameObject _obj3;
    private string _name;
    private int _win;
    private int _scene;

    private AudioSource _audioSource;
    public AudioClip _soundWin;
    public AudioClip _soundLose;

    // Start is called before the first frame update
    void Start()
    {
        _name = PlayerPrefs.GetString("nom");
        _win = PlayerPrefs.GetInt("win");
        _scene = PlayerPrefs.GetInt("scene");
        _obj = GameObject.Find("Name");
        _obj.GetComponent<Text>().text = _name;
        _obj3 = GameObject.Find("CountFire");
        _obj3.GetComponent<Text>().text = "Has aguantat "+ GameManager.Instance.NumBallsFire().ToString() + " boles de foc";

        _audioSource = GetComponent<AudioSource>();

        if (_scene != 10)
        {
            if (_win == 1)
            {
                _obj2 = GameObject.Find("HUD2");
                _obj2.SetActive(false);
                _audioSource.clip = _soundWin;
                _audioSource.Play();
            }
            else
            {
                _obj1 = GameObject.Find("HUD1");
                _obj1.SetActive(false);
                _audioSource.clip = _soundLose;
                _audioSource.Play();
            }
        }
        else
        {
            if (_win != 1)
            {
                _obj2 = GameObject.Find("HUD2");
                _obj2.SetActive(false);
                _audioSource.clip = _soundWin;
                _audioSource.Play();
            }
            else
            {
                _obj1 = GameObject.Find("HUD1");
                _obj1.SetActive(false);
                _audioSource.clip = _soundLose;
                _audioSource.Play();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
